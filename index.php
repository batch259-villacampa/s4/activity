<?php 
	session_start();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 04: Login page</title>
</head>
<body>
	<h1>Login</h1>
	<form method="POST" action="./process.php">
		<input type="hidden" name="action" value="login">
		<label for="email">Email Address:</label>
		<input type="email" name="email" id="email" required>
		<label for="password">Password:</label>
		<input type="password" name="password" id="password" required>
		<button type="submit">Login</button>
	</form>
</body>
</html>
