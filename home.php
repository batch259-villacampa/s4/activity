<?php 
	session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Home</title>
</head>
<body>
	<h1>Hello, <?= $_SESSION['email'] ?></h1>
	<form method="POST" action="./process.php">
		<input type="hidden" name="action" value="logout">
		<button type="submit">Logout</button>
	</form>
</body>
</html>
